<html>

namespace App\Components\Project;

use App\Components\Common\Log\GLogger;

class ProjectTable
{
    public static function getModel($table)
    {
        $modular_arr = [];
        switch ($table) {
@foreach($table_names as $key=>$table_name)
            case '{{$table_name}}':
                $modular_arr['model'] = new \App\Models\{{Str::studly($table_name)}}();
                $modular_arr['manager'] = new \App\Managers\{{Str::studly($table_name)}}Manager();
                break;
@endforeach
@foreach($common_modular_arr as $key=>$common_modular)
            case '{{$common_modular}}':
                $modular_arr['model'] = new \App\Models\Common\{{Str::studly($common_modular)}}();
                $modular_arr['manager'] = new \App\Managers\Common\{{Str::studly($common_modular)}}Manager();
            break;
@endforeach
@foreach($extra_arr as $key=>$extra)
            case '{{$extra['table']}}':
                $modular_arr['model'] = new {{$extra['model']}};
                $modular_arr['manager'] = new {{$extra['manager']}};
            break;
@endforeach
            default;
                GLogger::processLog(__METHOD__,$table.'表不存在','error');
        }
        return $modular_arr;
    }
}


</html>
