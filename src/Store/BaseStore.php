<?php

namespace Ldawn\Base\Store;

use Illuminate\Foundation\Providers\ComposerServiceProvider;
use Illuminate\Support\Arr;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BaseStore
{
    const NO_CREATE_TABLE_ARR = ['jobs',
        'migrations',
        'failed_jobs',
        'activity_history',
        'rbac_a_admin_power',
        'rbac_a_admin_role',
        'rbac_a_menu',
        'rbac_a_power',
        'rbac_a_role',
        'rbac_a_role_power',
        'admin',
        'login',
    ];

    const COMMON_MODULAR_ARR = [
        'admin',
        'columns',
        'login'
    ];

    const PATH_ARR = [
        'model' => 'app/Models/',
        'manager' => 'app/Managers/',
        'api_controller' => 'app/Http/Controllers/Api',
        'web_controller' => 'app/Http/Controllers/Web/Admin',
        'project_table' => 'app/Components/Project',
    ];

    const PATH_STORAGE_ARR = [
        'model' => 'Code\Models\Mysql\Stable',
        'manager' => 'Code\Managers\MySQL\Stable',
        'api_controller' => 'Code\Controllers\UserPortal\Stable',
        'web_controller' => 'Code\Controllers\AdminConsole\Stable',
        'project_table' => 'app/Components/Project/ProjectTable',
    ];

    const BASE_OPT_LIST = [
        'index' => '查看列表',
        'edit' => '编辑页面',
        'info' => '查看详情',
        'editPassword' => '修改密码页',
        'editPasswordPost' => '修改密码保存',
        'editMyself' => '修改个人信息',
        'editMyselfPost' => '修改个人信息保存',
        'statistic' => '统计',
        'editInfo' => '基础信息',
        'editInfoPost' => '基础信息保存',
        'select' => '选择',
        'batchDel' => '批量删除',
        'del' => '删除',
        'exportExcel' => '导出excel',
        'inputExcel' => '导入excel',
        'seqIndex' => '查看排名列表',
        'seqEdit' => '查看排名详情',
        'seqEditPost' => '修改排名',
        'setStatus' => '修改状态',
        'resetPassword' => '重置密码',
    ];

    //创建ProjectTable
    public static function createProjectTable($table_names = [])
    {
        if ($table_names == []) {
            $tables = DB::select("select TABLE_NAME,TABLE_COMMENT from information_schema.tables where TABLE_SCHEMA='" . config('database.connections.mysql.database') . "'");
            $table_names = [];
            foreach ($tables as $table) {
                $table_name = $table->TABLE_NAME;
                array_push($table_names, $table_name);
            }
            $table_names=array_diff($table_names,self::NO_CREATE_TABLE_ARR);
        }
        $param = [
            'table_names' => $table_names,
            'common_modular_arr' => self::COMMON_MODULAR_ARR,
        ];
        $extra_arr = [];
        if (self::hasComposerPackage('ldawn/activity-history')) {
            $activity_history_arr = [
                'table' => 'activity_history',
                'model' => '\Ldawn\ActivityHistory\Models\ActivityHistory',
                'manager' => '\Ldawn\ActivityHistory\Managers\ActivityHistoryManager',
            ];
            array_push($extra_arr, $activity_history_arr);
        }

        if (self::hasComposerPackage('ldawn/rbac-a')) {
            $rbac_a_arr = [
                'table' => 'rbac_a_admin_power',
                'model' => '\App\Models\RbacA\RbacAAdminPower',
                'manager' => '\App\Managers\RbacA\RbacAAdminPowerManager',
            ];
            array_push($extra_arr, $rbac_a_arr);

            $rbac_a_arr = [
                'table' => 'rbac_a_admin_role',
                'model' => '\App\Models\RbacA\RbacAAdminRole',
                'manager' => '\App\Managers\RbacA\RbacAAdminRoleManager',
            ];
            array_push($extra_arr, $rbac_a_arr);

            $rbac_a_arr = [
                'table' => 'rbac_a_menu',
                'model' => '\App\Models\RbacA\RbacAMenu',
                'manager' => '\App\Managers\RbacA\RbacAMenuManager',
            ];
            array_push($extra_arr, $rbac_a_arr);

            $rbac_a_arr = [
                'table' => 'rbac_a_power',
                'model' => '\App\Models\RbacA\RbacAPower',
                'manager' => '\App\Managers\RbacA\RbacAPowerManager',
            ];
            array_push($extra_arr, $rbac_a_arr);

            $rbac_a_arr = [
                'table' => 'rbac_a_role',
                'model' => '\App\Models\RbacA\RbacARole',
                'manager' => '\App\Managers\RbacA\RbacARoleManager',
            ];
            array_push($extra_arr, $rbac_a_arr);

            $rbac_a_arr = [
                'table' => 'rbac_a_role_power',
                'model' => '\App\Models\RbacA\RbacARolePower',
                'manager' => '\App\Managers\RbacA\RbacARolePowerManager',
            ];
            array_push($extra_arr, $rbac_a_arr);
        }

        $param['extra_arr'] = $extra_arr;
        $file_string = view('BaseView::project.project_table', $param)->__toString();
        $file_string = self::replaceTags($file_string);
        self::publishFile('project_table', $file_string, 'ProjectTable', 'force');
    }

    //替换标签
    public static function replaceTags($file_string)
    {
        $file_string = str_replace("<html>", "<?php", $file_string);
        $file_string = str_replace("</html>", "", $file_string);
        $file_string = str_replace("@replace_method", "@method", $file_string);     //主要是生成gcode文档
        return $file_string;
    }

    //发布文件
    public static function publishFile($path_type, $file_string, $file_name, $style)
    {
        $path = self::PATH_ARR[$path_type];
        $storage_path = self::PATH_STORAGE_ARR[$path_type];
        if ($style == 'force') {
            $myfile = fopen(base_path($path . '/' . $file_name . '.php'), "w");
            fwrite($myfile, $file_string);
            fclose($myfile);
        } elseif ($style == 'publish') {
            if (!file_exists(base_path($path . '/' . $file_name . '.php'))) {
                $myfile = fopen(base_path($path . '/' . $file_name . '.php'), "w");
                fwrite($myfile, $file_string);
                fclose($myfile);
            }
        } else {
            Storage::disk('local')->put($storage_path . '/' . $file_name . '.php', $file_string);
        }
    }

    public static function replaceJson($file_string)
    {
        $file_string = str_replace("{", "[", $file_string);
        $file_string = str_replace("[", "[
        ", $file_string);
        $file_string = str_replace("}", "]", $file_string);
        $file_string = str_replace("],", "
        ],
        ", $file_string);
        $file_string = str_replace(":", "=>", $file_string);
        return $file_string;
    }

    public static function u2c($str)
    {
        return preg_replace_callback("#\\\u([0-9a-f]{4})#i",

            function ($r) {
                return iconv('UCS-2BE', 'UTF-8', pack('H4', $r[1]));
            },
            $str);
    }

    public static function resetConfig($type)
    {
        Artisan::call('config:cache', []);
        $ldawn_config = [];
        if (file_exists(config_path('ldawn.php'))) {
            $ldawn_config = config('ldawn');
        }

        //处理模块列表
        $modular_list = [];
        $tables = DB::select("select TABLE_NAME,TABLE_COMMENT from information_schema.tables where TABLE_SCHEMA='" . config('database.connections.mysql.database') . "'");
        foreach ($tables as $table) {
            $table_name = $table->TABLE_NAME;
            $table_cn_name = ($table->TABLE_COMMENT) ? $table->TABLE_COMMENT : $table_name;
            $table_name = lcfirst(Str::studly($table_name));
            $modular_list[$table_name] = $table_cn_name;
        }
        $ldawn_config['modular_list'] = array_unique(Arr::get($ldawn_config, 'modular_list', []) + $modular_list);
        $no_create_modular = Arr::get($ldawn_config, 'no_create_modular', []);
        if ($no_create_modular == []) {
            $no_create_modular = self::NO_CREATE_TABLE_ARR;
        }
        $ldawn_config['no_create_modular'] = array_unique(Arr::get($ldawn_config, 'no_create_modular', []) + $no_create_modular);
        $ldawn_config['modular_list'] = array_diff($ldawn_config['modular_list'], $ldawn_config['no_create_modular']);
        ksort($ldawn_config['modular_list']);

        //处理操作列表
        $ldawn_config['opt_list'] = array_unique(Arr::get($ldawn_config, 'opt_list', []) + self::BASE_OPT_LIST);

        //处理activity-history相关配置
        if ($type == 'activity-history') {
            $record_method = ['DELETE',
                'PATCH',
                'POST',
                'GET'];
            if (Arr::get($ldawn_config, 'activity-history.record_method', []) == []) {
                $ldawn_config['activity-history']['record_method'] = $record_method;
            }
        }

        //处理rbac-a相关配置
        if ($type == 'rbac-a') {
            $no_power_modular = ['gcoder',
                'logout',
                'index'];

            if (Arr::get($ldawn_config, 'rbac-a.no_power_modular', []) == []) {
                $ldawn_config['rbac-a']['no_power_modular'] = $no_power_modular;
            }

            //操作列表
            $ldawn_config['opt_list'] = array_unique(Arr::get($ldawn_config, 'opt_list', []) + BaseStore::BASE_OPT_LIST);

            $ldawn_config['rbac-a']['status'] = "env('LDAWN_RBACA_STATUS',true)";
        }

        //开始生成
        $ldawn_config_string = view('BaseView::config.ldawn', ['ldawn_config' => $ldawn_config])->__toString();
        $ldawn_config_string = BaseStore::replaceTags($ldawn_config_string);
        $ldawn_config_string = BaseStore::replaceJson($ldawn_config_string);
        $ldawn_config_string = BaseStore::u2c($ldawn_config_string);
        $ldawn_config_string = BaseStore::specialOpt($ldawn_config_string);


        $myfile = fopen(config_path('ldawn.php'), "w");
        fwrite($myfile, $ldawn_config_string);
        fclose($myfile);

        Artisan::call('config:cache', []);
    }

    //判断某个composer包是否存在
    public static function hasComposerPackage($table_name)
    {
        $composer_package_arr = json_decode(file_get_contents(base_path('composer.json')), true)['require'];
        if (array_key_exists($table_name, $composer_package_arr)) {
            return true;
        }
        return false;
    }

    public static function specialOpt($file_string)
    {
        $file_string = str_replace('"env(\'LDAWN_RBACA_STATUS\',true)"', "env('LDAWN_RBACA_STATUS',true)", $file_string);
        return $file_string;
    }
}
